from api.models.investimento_models import (
    Posicao, Portfolio, Ativo, TipoAtivo, EmpresaBolsa,
    Mercado, Setor, Industria, Bolsa
)
from rest_framework import serializers


class PosicaoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Posicao
        fields = ('__all__')

class PortfolioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Portfolio
        fields = ('__all__')

class TipoAtivoSerializer(serializers.ModelSerializer):

    class Meta:
        model = TipoAtivo
        fields = ('descricao', )

class EmpresaBolsaSerializer(serializers.ModelSerializer):

    class Meta:
        model = EmpresaBolsa
        fields = ('descricao', )

class MercadoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mercado
        fields = ('descricao', )

class SetorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Setor
        fields = ('descricao', )

class IndustriaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Industria
        fields = ('descricao', )

class BolsaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bolsa
        fields = ('descricao', )

class AtivoSerializer(serializers.ModelSerializer):

    tipo_ativo = TipoAtivoSerializer()
    empresa = EmpresaBolsaSerializer()
    mercado = MercadoSerializer()
    setor = SetorSerializer()
    industria = IndustriaSerializer()
    bolsa = BolsaSerializer()

    class Meta:
        model = Ativo
        fields = (
            'id', 'descricao', 'codigo', 'tipo_ativo', 'empresa', 'mercado',
            'setor', 'industria', 'bolsa',
        )

from api.domain.financa.entities.movimentacao_entity import MovimentacaoEntity
from api.domain.financa.repositories.movimentacao_repository import MovimentacaoRepository
from api.domain.financa.enums.status_movimentacao_enum import StatusMovimentacaoEnum
from api.domain.financa.clients.movimentacao_sheet_client import MovimentacaoSheetClient
from datetime import datetime


class MovimentacaoService:
    def __init__(self):
        self.movimentacao_repository = MovimentacaoRepository()

    def cria_movimentacao(self, movimentacao):
        movimentacao['data_cadastro'] = datetime.now()
        movimentacao['status_movimentacao'] = int(StatusMovimentacaoEnum.AGUARDANDO)
        movimentacao = self.movimentacao_repository.cria_movimentacao(movimentacao)

        return movimentacao

    def cria_movimentacao_bulk(self, movimentacoes):
        self.movimentacao_repository.cria_movimentacao_bulk(movimentacoes)

        return None

    def atualiza_movimentacao(self, movimentacao_id, movimentacao):
        movimentacao['data_atualizacao'] = datetime.now()
        movimentacao = self.movimentacao_repository.atualiza_movimentacao(movimentacao_id, movimentacao)

        return movimentacao

    def consulta_movimentacao_todos(self):
        movimentacoes = self.movimentacao_repository.consulta_movimentacao_todos()

        return movimentacoes

    def consulta_movimentacao_por_id(self, movimentacao_id):
        movimentacao = self.movimentacao_repository.consulta_movimentacao_por_id(movimentacao_id)

        return movimentacao

    def sincroniza_movimentacao_sheet(self):
        movimentacoes_sheet = MovimentacaoSheetClient().consulta_movimentacoes()
        movimentacoes = []

        for mov in movimentacoes_sheet:
            movimentacao = MovimentacaoEntity().create_from_movimentacao_sheet(mov)
            movimentacoes.append(movimentacao)

        self.cria_movimentacao_bulk(movimentacoes)
        print('teste')

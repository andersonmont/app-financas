from api.models.financa_models import Movimentacao, Categoria
from api.domain.financa.enums.tipo_movimentacao_enum import TipoMovimentacaoEnum
from api.domain.financa.enums.categoria_enum import CategoriaEnum
from api.domain.financa.enums.moeda_enum import MoedaEnum
from api.domain.financa.converters.categoria_converter import CategoriaSwitch
from api.domain.financa.converters.tipo_movimentacao_converter import TipoMovimentacaoSwitch
from api.domain.financa.converters.status_movimentacao_converter import StatusMovimentacaoSwitch
import uuid
import unicodedata
import datetime


class MovimentacaoEntity:

    def create_from_movimentacao_sheet(self, movimentacao_sheet):
        categoria_id = CategoriaSwitch().convert_from_sheet(movimentacao_sheet['categoria'])
        tipo_movimentacao_id = TipoMovimentacaoSwitch().convert_from_sheet(movimentacao_sheet['tipo_movimentacao'])
        status_id = StatusMovimentacaoSwitch().convert_from_sheet(movimentacao_sheet['status'])

        movimentacao = Movimentacao(
            id=uuid.UUID(movimentacao_sheet['id']),
            descricao=movimentacao_sheet['descricao'],
            tipo_movimentacao_id=tipo_movimentacao_id,
            status_movimentacao_id=status_id,
            data_cadastro=datetime.datetime.now(),
            categoria_id=categoria_id,
            moeda_id=int(MoedaEnum.REAL)
        )

        if movimentacao_sheet['data_realizacao']:
            movimentacao.data_realizacao = datetime.datetime.strptime(movimentacao_sheet['data_realizacao'], '%d/%m/%Y').strftime('%Y-%m-%d')

        if movimentacao_sheet['data_vencimento']:
            movimentacao.data_vencimento = datetime.datetime.strptime(movimentacao_sheet['data_vencimento'], '%d/%m/%Y').strftime('%Y-%m-%d')

        if movimentacao_sheet['parcela_atual'] and isinstance(movimentacao_sheet['parcela_atual'], int):
            movimentacao.parcela_atual = movimentacao_sheet['parcela_atual']

        if movimentacao_sheet['parcela_final'] and isinstance(movimentacao_sheet['parcela_final'], int):
            movimentacao.parcela_final = movimentacao_sheet['parcela_final']

        if movimentacao_sheet['valor_processar']:
            valor_devido = str(movimentacao_sheet['valor_processar']).replace('.', '')
            valor_devido = valor_devido.replace('R$ ', '')
            movimentacao.valor_devido = float(valor_devido.replace(',', '.'))

        if movimentacao_sheet['data_pagamento']:
            movimentacao.data_pagamento = datetime.datetime.strptime(movimentacao_sheet['data_pagamento'], '%d/%m/%Y').strftime('%Y-%m-%d')

        if movimentacao_sheet['valor_processado']:
            valor_processado = str(movimentacao_sheet['valor_processado']).replace('.', '')
            valor_processado = valor_devido.replace('R$ ', '')
            movimentacao.valor_pago = float(valor_processado.replace(',', '.'))

        return movimentacao

from enum import IntEnum


class TipoPosicaoEnum(IntEnum):
    COMPRA = 1,
    VENDA = 2

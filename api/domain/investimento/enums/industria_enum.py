from enum import IntEnum

class IndustriaEnum(IntEnum):
    BENS_DE_CAPITAL = 1
    CONSUMO_CICLICO	= 2
    CONSUMO_NAO_CICLICO	= 3
    ENERGIA	= 4
    FINANCEIRO	= 5
    MATERIAIS_BASICOS = 6
    SAUDE = 7
    SERVICOS = 8
    TECNOLOGIA = 9
    TRANSPORTE = 10
    UTILIDADES = 11

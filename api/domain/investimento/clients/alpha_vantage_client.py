from api.domain.investimento.providers.alpha_vantage_provider import AlphaVantageRepository
from api.models.investimento_models import Cotacao
from api.domain.investimento.entities.moeda_enum import MoedaEnum
from api.domain.investimento.entities.investimento_entities import AtivoSwitch
import datetime


class AlphaVantageClient:

    def __init__(self):
        self.alpha_vantage_repository = AlphaVantageRepository()

    def search_time_series_intraday(self, intervalo, codigo):
        time_series_intraday = self.alpha_vantage_repository.search_time_series_intraday(
            intervalo, codigo)

        cotacoes = self.time_series_to_model(
            time_series_intraday, codigo, True)

        return cotacoes

    def search_time_series_daily(self, codigo):
        time_series_daily = self.alpha_vantage_repository.search_time_series_daily(
            codigo)

        cotacoes = self.time_series_to_model(time_series_daily, codigo)

        return cotacoes

    def search_time_series_daily_adjusted(self, codigo):
        time_series_daily_adjusted = self.alpha_vantage_repository.search_time_series_daily_adjusted(
            codigo)

        cotacoes = self.time_series_to_model(
            time_series_daily_adjusted, codigo)

        return cotacoes

    def search_time_series_latest(self, codigo):
        time_series_latest = self.alpha_vantage_repository.search_time_series_latest(
            codigo)

        cotacao = self.time_series_simple_to_model(
            time_series_latest['Global Quote'], codigo)

        return cotacao

    def time_series_to_model(self, time_series, codigo, intraday=False):
        cotacoes = []
        time_series_resumed = list(dict([value for key, value in dict(
            time_series).items() if 'Time Series' in key][0]).items())

        for time_series in time_series_resumed:

            cotacao_atual = Cotacao(
                ativo_id=AtivoSwitch().codigo_switch(codigo),
                data_pregao=datetime.datetime.fromisoformat(time_series[0]).date(),
                moeda_id=int(MoedaEnum.REAL),
                preco_abertura=float(time_series[1]['1. open']),
                preco_maximo=float(time_series[1]['2. high']),
                preco_minimo=float(time_series[1]['3. low']),
                preco_ultimo=float(time_series[1]['4. close']),
                volume=float(time_series[1]['5. volume'])
            )
            if intraday:
                cotacao_atual.data_hora_pregao = datetime.datetime.fromisoformat(
                    time_series[0])

            cotacoes.append(cotacao_atual)

        return cotacoes

    def time_series_simple_to_model(self, time_series_simple, codigo):

        cotacao = Cotacao(
            ativo_id=AtivoSwitch().codigo_switch(codigo),
            data_pregao=time_series_simple['07. latest trading day'],
            moeda_id=int(MoedaEnum.REAL),
            preco_abertura=float(time_series_simple['02. open']),
            preco_atual=float(time_series_simple['05. price']),
            preco_maximo=float(time_series_simple['03. high']),
            preco_minimo=float(time_series_simple['04. low']),
            preco_ultimo=float(time_series_simple['05. price']),
            volume=float(time_series_simple['06. volume'])
        )

        return cotacao

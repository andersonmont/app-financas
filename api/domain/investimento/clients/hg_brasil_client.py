from api.domain.investimento.providers.hg_brasil_provider import HgBrasilRepository
from api.models.investimento_models import Cotacao
from api.domain.investimento.entities.moeda_enum import MoedaEnum
from api.domain.investimento.entities.investimento_entities import AtivoSwitch


class HgBrasilClient:

    def __init__(self):
        self.hg_brasil_repository = HgBrasilRepository()

    def get_stock_price_by_symbol(self, codigo):
        stock_price = self.hg_brasil_repository.get_stock_price_by_symbol(codigo)

        cotacoes = self.covert_hg_brasil_to_model(stock_price)

        return cotacoes

    def get_finance_info(self):
        finance_info = self.hg_brasil_repository.get_finance_info()

        return finance_info

    def covert_hg_brasil_to_model(self, hg_brasil_prices):
        cotacoes = []

        high_or_lor_prices_list = list(dict(hg_brasil_prices).items())

        for hg_brasil_price in high_or_lor_prices_list:
            cotacao = Cotacao(
                ativo_id=AtivoSwitch().codigo_switch(hg_brasil_price[0]),
                moeda_id=int(MoedaEnum.REAL),
                preco_atual=float(hg_brasil_price[1]['price']),
                valor_mercado=float(hg_brasil_price[1]['market_cap'])
            )

            cotacoes.append(cotacao)

        return cotacoes

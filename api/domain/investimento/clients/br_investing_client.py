from api.domain.investimento.providers.br_investing_provider import BrInvestingRepository
from api.models.investimento_models import Cotacao
from api.domain.investimento.entities.moeda_enum import MoedaEnum
from api.domain.investimento.entities.investimento_entities import AtivoSwitch


class BrInvestingService:

    def __init__(self):
        self.br_investing_repository = BrInvestingRepository()
    
    def get_overview_stock_report(self):
        overview_stock_report = self.br_investing_repository.get_stock_report_('overview')

        cotacoes = self.convert_br_investing_stock_to_model(overview_stock_report)

        return cotacoes

    def convert_br_investing_stock_to_model(self, overview_stock_report):
        cotacoes = []

        for stock in overview_stock_report:
            preco_atual = stock['Último'].replace('.', '')
            volume = stock['Vol.'].replace('.', '')

            cotacao = Cotacao(
                ativo_id=AtivoSwitch().codigo_switch(stock['Códigos']),
                moeda_id=int(MoedaEnum.REAL),
                preco_atual=float(preco_atual.replace(',', '.')),
                volume=float(volume.replace(',', '.'))
            )
            cotacoes.append(cotacao)

        return cotacoes

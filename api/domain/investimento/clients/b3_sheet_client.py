from api.domain.investimento.providers.b3_sheet_provider import B3SheetRepository
from api.models.investimento_models import Cotacao
from api.domain.investimento.entities.moeda_enum import MoedaEnum
from api.domain.investimento.entities.investimento_entities import AtivoSwitch


class B3SheetClient:

    def __init__(self):
        self.bovespa_sheet_repository = B3SheetRepository()

    def read_historical_by_stock(self, full_path, codigo):
        cotacoes = []

        historical_stock = self.bovespa_sheet_repository.read_historical_by_stock(
            full_path, codigo
        )

        for stock in historical_stock:
            cotacao = self.convert_b3_to_model(stock, codigo)

            if not cotacao:
                continue

            cotacoes.append(cotacao)

        return cotacoes

    def read_historical_full(self, full_path):
        cotacoes = []

        historical_full = self.bovespa_sheet_repository.read_historical_full(
            full_path
        )

        for stock in historical_full:
            cotacao = self.convert_b3_to_model(stock, stock.stock_code)

            if not cotacao:
                continue

            cotacoes.append(cotacao)

        return cotacoes

    def convert_b3_to_model(self, b3_stock, codigo):
        ativo_id = AtivoSwitch().codigo_switch(codigo)

        if not ativo_id:
            return False

        cotacao = Cotacao(
            ativo_id=AtivoSwitch().codigo_switch(codigo),
            data_pregao=b3_stock.date,
            moeda_id=int(MoedaEnum.REAL),
            preco_abertura=float(b3_stock.price_open),
            preco_maximo=float(b3_stock.price_high),
            preco_minimo=float(b3_stock.price_low),
            preco_ultimo=float(b3_stock.price_close),
            volume=float(b3_stock.volume),
            quantidade=float(b3_stock.quantity)
        )

        return cotacao

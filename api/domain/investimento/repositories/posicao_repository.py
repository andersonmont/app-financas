from api.models.investimento_models import Posicao
from api.serializers.investimento_serializers import PosicaoSerializer
from django.core.exceptions import ObjectDoesNotExist


class PosicaoRepository:

    def consulta_posicoes(self):
        posicoes = Posicao.objects.all()

        return posicoes

    def consulta_posicao_por_id(self, posicao_id):
        posicao = Posicao.objects.filter(pk=posicao_id).first()

        return posicao

    def cria_posicao(self, posicao):
        serializer = PosicaoSerializer(data=posicao)

        if serializer.is_valid():
            serializer.save()

        return serializer.data

    def atualiza_posicao(self, posicao_id, posicao):
        try:
            posicao_existente = Posicao.objects.get(pk=posicao_id)
        except ObjectDoesNotExist:
            return None

        serializer = PosicaoSerializer(
            posicao_existente, data=posicao, partial=True
        )

        if serializer.is_valid():
            serializer.save()

        return serializer.data

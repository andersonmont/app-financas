from api.models.investimento_models import Portfolio
from api.serializers.investimento_serializers import PortfolioSerializer
from django.core.exceptions import ObjectDoesNotExist


class PortfolioRepository:

    def consulta_portfolios(self):
        portfolios = Portfolio.objects.all()

        return portfolios

    def consulta_portfolio_por_id(self, portfolio_id):
        portfolio = Portfolio.objects.filter(pk=portfolio_id).first()

        return portfolio

    def cria_portfolio(self, portfolio):
        serializer = PortfolioSerializer(data=portfolio)

        if serializer.is_valid():
            serializer.save()

        return serializer.data

    def atualiza_portfolio(self, portfolio_id, portfolio):
        try:
            portfolio_existente = Portfolio.objects.get(pk=portfolio_id)
        except ObjectDoesNotExist:
            return None

        serializer = PortfolioSerializer(
            portfolio_existente, data=portfolio, partial=True
        )

        if serializer.is_valid():
            serializer.save()

        return serializer.data

import ssl
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
import time
import yfinance as yf

# For ignoring SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

__cache__ = {}

class YahooFinanceRepository:

    def get_price_from_web_by_stock(self, ticker):
        if ticker in __cache__:
            return __cache__[ticker]
        try:
            ticker_sa = ticker + '.SA'
            url = "http://finance.yahoo.com/quote/%s?p=%s" % (
                ticker_sa, ticker_sa)

            # Making the website believe that you are accessing it using a Mozilla browser
            req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
            webpage = urlopen(req).read()

            time.sleep(1)

            soup = BeautifulSoup(webpage, 'html.parser')
            # html = soup.prettify('utf-8')

            for span in soup.findAll('span', attrs={'class': 'Trsdu(0.3s) Trsdu(0.3s) Fw(b) Fz(36px) Mb(-4px) D(b)'}):
                preco_atual = float(span.text.replace(',', '').strip())
                __cache__[ticker] = preco_atual
                return preco_atual
            raise Exception('Preco ticker nao encontrado ' + ticker)
        except Exception as ex:
            raise Exception('Preco ticker nao encontrado ' + ticker, ex)

    def get_historical_data(self):
        msft = yf.Ticker("AZUL4.SA")

        # get stock info
        msft.info

        # get historical market data
        hist = msft.history(period="max")

        # show actions (dividends, splits)
        actions = msft.actions

        # show dividends
        dividends = msft.dividends

        # show splits
        splits = msft.splits

        # show financials
        financials = msft.financials
        quarterly_financials = msft.quarterly_financials

        # show major holders
        major_holders = msft.major_holders

        # show institutional holders
        institutional_holders = msft.institutional_holders

        # show balance heet
        balance_sheet = msft.balance_sheet
        quarterly_balance_sheet = msft.quarterly_balance_sheet

        # show cashflow
        cashflow = msft.cashflow
        quarterly_cashflow = msft.quarterly_cashflow

        # show earnings
        earnings = msft.earnings
        quarterly_earnings = msft.quarterly_earnings

        # show sustainability
        sustainability = msft.sustainability

        # show analysts recommendations
        recommendations = msft.recommendations

        # show next event (earnings, etc)
        calendar = msft.calendar

        # show ISIN code - *experimental*
        # ISIN = International Securities Identification Number
        isin = msft.isin

        # show options expirations
        options = msft.options

        return None

import bovespa as bov_sheet


class B3SheetRepository:

    def read_historical_by_stock(self, full_path, codigo):
        resp = []

        bovespa_historical_stock = bov_sheet.File(full_path).query(stock=codigo)

        for bovespa_stock in bovespa_historical_stock:
            resp.append(bovespa_stock)

        return resp

    def read_historical_full(self, full_path):
        resp = []

        bovespa_historical_full = bov_sheet.File(full_path).query()

        for bovespa_stock in bovespa_historical_full:
            resp.append(bovespa_stock)

        return resp

import requests
import csv
from io import StringIO


class BrInvestingRepository:

    def __init__(self):
        self.url = 'https://br.investing.com/stock-screener/Service/downloadData?download=1&country%5B%5D=32&exchange%5B%5D=47&exchange%5B%5D=113&order%5Bcol%5D=eq_market_cap&order%5Bdir%5D=d&tab={report_type}&pn='
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36'
        }

    def get_stock_report_(self, report_type):
        """
        Args:
            report_type: overview, fundamental, technical, performance
        """
        url = self.url.format(report_type=report_type)
        stock_list = []
        page_max = 2

        for page in range(1, page_max):
            response = requests.get((url+str(page)), headers=self.headers)

            if response.status_code != 200:
                return False

            f = StringIO(response.text.replace("\ufeff", ""))
            reader = csv.DictReader(f, delimiter=',')

            for row in reader:
                stock_list.append(row)

        return stock_list

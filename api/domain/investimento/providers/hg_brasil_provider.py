import requests
import json


class HgBrasilRepository:

    def __init__(self):
        self.url = 'https://api.hgbrasil.com/finance'
        self.api_key = '08da39c3'

    def get_stock_price_by_symbol(self, codigo):
        """Com esse endpoint você pode obter o preço de uma ação listada
        no IBOVESPA através do símbolo desse título.
        """

        url = self.url + '/stock_price?key={}&symbol={}'.format(
            self.api_key, codigo
        )

        response = requests.get(url)

        if response.status_code != 200:
            return False

        resp_json = json.loads(response.content)

        return resp_json['results']

    def get_finance_info(self):
        """Com este método, todos os dados são retornados em uma só requisição.

        Returns:
            Câmbio das moedas: Dólar USD, Euro EUR, Libra (GBP), Peso (ARS) e Bitcoin (BTC) para Real (BRL);
            Valores de exchanges de Bitcoin (BTC) como Coinbase, Blockchain.info, BitStamp, FoxBit e Mercado Bitcoin;
            Variação das bolsas de valores: IBOVESPA, NASDAQ, CAC 40 e NIKKEI;
            Taxas brasileiras: CDI, SELIC e fator diário.
        """
        url = self.url + '?key={}'.format(self.api_key)

        response = requests.get(url)

        if response.status_code != 200:
            return False

        resp_json = json.loads(response.content)

        return resp_json['results']

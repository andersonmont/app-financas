import requests
import json


class AlphaVantageRepository:
    """ This suite of APIs provide realtime and historical global equity data in 4
        different temporal resolutions: (1) daily, (2) weekly, (3) monthly, and (4)
        intraday. Daily, weekly, and monthly time series contain 20+ years of historical data..

    Arg:
        function: the time series of your choice.
        symbol: the name of the equity of your choice.
        interval: time interval between two consecutive data points in the time series.
            The following values are supported: 1min, 5min, 15min, 30min, 60min
        outputsize: by default, outputsize=compact. Strings compact and full are accepted with the
            following specifications: compact returns only the latest 100 data points
            in the intraday time series; full returns the full-length intraday time series. The
            "compact" option is recommended if you would like to reduce the data size of each API call.
    """

    def __init__(self):
        self.url = 'https://www.alphavantage.co/query?'
        self.api_key = 'EYVAOSJK8915M9QO'

    def search_time_series_intraday(self, intervalo, codigo):
        funcao = 'TIME_SERIES_INTRADAY'
        intervalo = str(intervalo) + 'min'

        url = self.url + 'apikey={}&function={}&interval={}&symbol={}.SA'.format(
            self.api_key, funcao, intervalo, codigo
        )

        response = requests.get(url)

        if response.status_code != 200:
            return False

        resp_json = json.loads(response.content)

        return resp_json

    def search_time_series_daily(self, codigo):
        funcao = 'TIME_SERIES_DAILY'

        url = self.url + 'apikey={}&function={}&symbol={}.SA'.format(
            self.api_key, funcao, codigo
        )

        response = requests.get(url)

        if response.status_code != 200:
            return False

        resp_json = json.loads(response.content)

        return resp_json

    def search_time_series_daily_adjusted(self, codigo):
        funcao = 'TIME_SERIES_DAILY_ADJUSTED'

        url = self.url + 'apikey={}&function={}&symbol={}'.format(
            self.api_key, funcao, codigo
        )

        response = requests.get(url)

        if response.status_code != 200:
            return False

        resp_json = json.loads(response.content)

        return resp_json

    def search_time_series_latest(self, codigo):
        funcao = 'GLOBAL_QUOTE'

        url = self.url + 'apikey={}&function={}&symbol={}.SA'.format(
            self.api_key, funcao, codigo
        )

        response = requests.get(url)

        if response.status_code != 200:
            return False

        resp_json = json.loads(response.content)

        return resp_json

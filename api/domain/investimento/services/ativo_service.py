from api.domain.investimento.repositories.ativo_repository import AtivoRepository


class AtivoService:

    def __init__(self):
        self.ativo_repository = AtivoRepository()

    def consulta_ativos(self):
        ativos = self.ativo_repository.consulta_ativos()

        return ativos

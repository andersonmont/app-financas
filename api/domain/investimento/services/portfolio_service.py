from api.domain.investimento.repositories.portfolio_repository import PortfolioRepository
import datetime


class PortfolioService:

    def __init__(self):
        self.portfolio_repository = PortfolioRepository()

    def consulta_portfolios(self):
        portfolios = self.portfolio_repository.consulta_portfolios()

        return portfolios

    def consulta_portfolio_por_id(self, portfolio_id):
        portfolio = self.portfolio_repository.consulta_portfolio_por_id(portfolio_id)

        return portfolio

    def cria_portfolio(self, portfolio):
        portfolio['data_cadastro'] = datetime.datetime.now()        
        portfolio = self.portfolio_repository.cria_portfolio(portfolio)

        return portfolio

    def atualiza_portfolio(self, portfolio_id, portfolio):
        portfolio['data_atualizacao'] = datetime.datetime.now()
        portfolio = self.portfolio_repository.atualiza_portfolio(portfolio_id, portfolio)

        return portfolio

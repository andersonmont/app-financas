from api.domain.investimento.repositories.posicao_repository import PosicaoRepository
import datetime


class PosicaoService:

    def __init__(self):
        self.posicao_repository = PosicaoRepository()

    def consulta_posicoes(self):
        posicoes = self.posicao_repository.consulta_posicoes()

        return posicoes

    def consulta_posicao_por_id(self, posicao_id):
        posicao = self.posicao_repository.consulta_posicao_por_id(posicao_id)

        return posicao

    def cria_posicao(self, posicao):
        posicao['data_cadastro'] = datetime.datetime.now()
        posicao = self.posicao_repository.cria_posicao(posicao)

        return posicao

    def atualiza_posicao(self, posicao_id, posicao):
        posicao['data_atualizacao'] = datetime.datetime.now()
        posicao = self.posicao_repository.atualiza_posicao(posicao_id, posicao)

        return posicao

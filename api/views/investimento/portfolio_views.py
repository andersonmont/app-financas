from api.domain.investimento.services.portfolio_service import PortfolioService
from api.serializers.investimento_serializers import PortfolioSerializer
from api.filters.investimento_filters import PortfolioFilter
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from rest_framework.views import APIView
from rest_framework import filters
from django_filters import rest_framework as drf_filters


class PortfolioCreateList(APIView):

    portfolio_service = PortfolioService()
    queryset = portfolio_service.consulta_portfolios()

    filter_backends = (filters.SearchFilter, drf_filters.DjangoFilterBackend,)
    search_fields = ['', ]
    filterset_class = PortfolioFilter

    def filter_queryset(self, queryset):
        for backend in list(self.filter_backends):
            queryset = backend().filter_queryset(self.request, queryset, self)
        return queryset

    def get_queryset(self):
        return self.queryset

    def get(self, request):
        paginator = PageNumberPagination()

        queryset = self.filter_queryset(self.get_queryset())
        page = paginator.paginate_queryset(queryset, request)

        serializer = PortfolioSerializer(page, many=True)

        if page is not None:
            return paginator.get_paginated_response(serializer.data)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = PortfolioSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        portfolio = self.portfolio_service.cria_portfolio(request.data)

        return Response(portfolio, status=status.HTTP_201_CREATED)

class PortfolioUpdateRetrieve(APIView):

    def __init__(self):
        self.portfolio_service = PortfolioService()

    def get(self, request, pk):
        portfolio = self.portfolio_service.consulta_portfolio_por_id(pk)

        if portfolio is None:
            return Response('error: portfolio não encontrado.', status=status.HTTP_404_NOT_FOUND)

        serializer = PortfolioSerializer(portfolio)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, pk):
        serializer = PortfolioSerializer(data=request.data, partial=True)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        serialized = self.portfolio_service.atualiza_portfolio(pk, request.data)

        return Response(serialized, status=status.HTTP_201_CREATED)

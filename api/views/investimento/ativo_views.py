from api.domain.investimento.services.ativo_service import AtivoService
from api.serializers.investimento_serializers import AtivoSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from rest_framework.views import APIView
from rest_framework import filters


class AtivoList(APIView):

    ativo_service = AtivoService()
    data = ativo_service.consulta_ativos()

    filter_backends = (filters.SearchFilter,)
    search_fields = ['descricao', 'codigo', ]

    def filter_queryset(self, queryset):
        for backend in list(self.filter_backends):
            queryset = backend().filter_queryset(self.request, queryset, self)
        return queryset

    def get_queryset(self):
        return self.data

    def get(self, request):
        paginator = PageNumberPagination()

        data = self.filter_queryset(self.get_queryset())
        page = paginator.paginate_queryset(data, request)

        serializer = AtivoSerializer(page, many=True)

        if page is not None:
            return paginator.get_paginated_response(serializer.data)

        return Response(serializer.data, status=status.HTTP_200_OK)

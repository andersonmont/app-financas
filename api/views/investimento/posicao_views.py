from api.domain.investimento.services.posicao_service import PosicaoService
from api.serializers.investimento_serializers import PosicaoSerializer
from api.filters.investimento_filters import PosicaoFilter
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from rest_framework.views import APIView
from rest_framework import filters
from django_filters import rest_framework as drf_filters


class PosicaoCreateList(APIView):

    posicao_service = PosicaoService()
    queryset = posicao_service.consulta_posicoes()

    filter_backends = (filters.SearchFilter, drf_filters.DjangoFilterBackend,)
    search_fields = ['', ]
    filterset_class = PosicaoFilter

    def filter_queryset(self, queryset):
        for backend in list(self.filter_backends):
            queryset = backend().filter_queryset(self.request, queryset, self)
        return queryset

    def get_queryset(self):
        return self.queryset

    def get(self, request):
        paginator = PageNumberPagination()

        queryset = self.filter_queryset(self.get_queryset())
        page = paginator.paginate_queryset(queryset, request)

        serializer = PosicaoSerializer(page, many=True)

        if page is not None:
            return paginator.get_paginated_response(serializer.data)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = PosicaoSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        posicao = self.posicao_service.cria_posicao(request.data)

        return Response(posicao, status=status.HTTP_201_CREATED)

class PosicaoUpdateRetrieve(APIView):

    def __init__(self):
        self.posicao_service = PosicaoService()

    def get(self, request, pk):
        posicao = self.posicao_service.consulta_posicao_por_id(pk)

        if posicao is None:
            return Response('error: posicao não encontrada.', status=status.HTTP_404_NOT_FOUND)

        serializer = PosicaoSerializer(posicao)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, pk):
        serializer = PosicaoSerializer(data=request.data, partial=True)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        serialized = self.posicao_service.atualiza_posicao(pk, request.data)

        return Response(serialized, status=status.HTTP_201_CREATED)

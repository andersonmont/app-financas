# Generated by Django 3.1.3 on 2020-11-30 23:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20201125_1930'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='EmpresaAtivo',
            new_name='EmpresaBolsa',
        ),
        migrations.AlterModelOptions(
            name='movimentacao',
            options={'ordering': ('data_atualizacao',)},
        ),
        migrations.RenameField(
            model_name='empresabolsa',
            old_name='descricao_b3',
            new_name='descricao_bolsa',
        ),
        migrations.AddField(
            model_name='portfolio',
            name='perfil_responsavel',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='api.perfil'),
        ),
        migrations.AlterField(
            model_name='historical_movimentacao',
            name='prioridade',
            field=models.ForeignKey(blank=True, db_constraint=False, default=2, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='api.prioridademovimentacao'),
        ),
        migrations.AlterField(
            model_name='movimentacao',
            name='perfil_responsavel',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='responsavel_movimentacao', to='api.perfil'),
        ),
        migrations.AlterField(
            model_name='movimentacao',
            name='prioridade',
            field=models.ForeignKey(blank=True, default=2, on_delete=django.db.models.deletion.PROTECT, to='api.prioridademovimentacao'),
        ),
        migrations.AlterModelTable(
            name='empresabolsa',
            table='empresa_bolsa',
        ),
    ]

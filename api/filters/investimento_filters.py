from api.models.investimento_models import (
    Posicao, Portfolio, Ativo
)
from django_filters import rest_framework as filtersdrf
import django_filters


class NumberInFilter(django_filters.BaseInFilter, django_filters.NumberFilter):
    pass


class TextInFilter(django_filters.BaseInFilter, django_filters.CharFilter):
    pass


class PosicaoFilter(django_filters.FilterSet):

    class Meta:
        model = Posicao
        fields = []


class PortfolioFilter(django_filters.FilterSet):

    class Meta:
        model = Portfolio
        fields = []

from django.urls import path
from api.views.financa import (
    auxiliar_views, cartao_views, comentario_views, conta_views,
    endereco_views, movimentacao_views, perfil_views
)
from api.views.investimento import (ativo_views, portfolio_views, posicao_views)

urlpatterns = [
    # Financa Pessoal
    path('moedas', auxiliar_views.MoedaList.as_view()),
    path('tipos-movimentacao', auxiliar_views.TipoMovimentacaoList.as_view()),
    path('formas-pagamento', auxiliar_views.FormaPagamentoList.as_view()),
    path('status-movimentacao', auxiliar_views.StatusMovimentacaoList.as_view()),
    path('categorias', auxiliar_views.CategoriaList.as_view()),
    path('tipos-conta', auxiliar_views.TipoContaList.as_view()),
    path('instituicoes-financeiras', auxiliar_views.InstituicaoFinanceiraList.as_view()),
    path('bandeiras', auxiliar_views.BandeiraList.as_view()),
    path('prioridades-movimentacao', auxiliar_views.PrioridadeMovimentacaoList.as_view()),
    path('enderecos', endereco_views.EnderecoCreateListView.as_view()),
    path('enderecos/<int:pk>', endereco_views.EnderecoRetrievelUpdateView.as_view()),
    path('contas', conta_views.ContaCreateList.as_view()),
    path('contas/<int:pk>', conta_views.ContaUpdateRetrieve.as_view()),
    path('movimentacoes', movimentacao_views.MovimentacaoCreateList.as_view()),
    path('movimentacoes/<str:pk>', movimentacao_views.MovimentacaoUpdateRetrieve.as_view()),
    path('perfis', perfil_views.PerfilCreateListView.as_view()),
    path('perfis/<int:pk>', perfil_views.PerfilRetrieveUpdateView.as_view()),
    path('comentarios', comentario_views.ComentarioCreateList.as_view()),
    path('comentarios/<int:pk>', comentario_views.ComentarioUpdateRetrieve.as_view()),
    path('cartoes', cartao_views.CartaoCreateList.as_view()),
    path('cartoes/<int:pk>', cartao_views.CartaoUpdateRetrieve.as_view()),
    path('movimentacoes/sincroniza-planilha', movimentacao_views.SyncMovimentacaoSheet.as_view()),
    # Investimento
    path('posicoes', posicao_views.PosicaoCreateList.as_view()),
    path('posicoes/<int:pk>', posicao_views.PosicaoUpdateRetrieve.as_view()),
    path('portfolios', portfolio_views.PortfolioCreateList.as_view()),
    path('portfolios/<int:pk>', portfolio_views.PortfolioUpdateRetrieve.as_view()),
    path('ativos', ativo_views.AtivoList.as_view()),
]

from django.db import models
from api.models.financa_models import Moeda, Conta, Perfil


class Setor(models.Model):
    descricao = models.CharField(max_length=100, null=False)

    class Meta:
        db_table = 'setor'


class Industria(models.Model):
    descricao = models.CharField(max_length=100, null=False)

    class Meta:
        db_table = 'industria'


class TipoAtivo(models.Model):
    descricao = models.CharField(max_length=100, null=False)

    class Meta:
        db_table = 'tipo_ativo'


class Mercado(models.Model):
    descricao = models.CharField(max_length=100, null=False)

    class Meta:
        db_table = 'mercado'


class TipoPosicao(models.Model):
    descricao = models.CharField(max_length=100, null=False)

    class Meta:
        db_table = 'tipo_posicao'


class Bolsa(models.Model):
    descricao = models.CharField(max_length=100, null=False)

    class Meta:
        db_table = 'bolsa'


class EmpresaBolsa(models.Model):
    nome_amigavel = models.CharField(max_length=500, null=False)
    descricao_bolsa = models.CharField(max_length=500, null=False)

    class Meta:
        db_table = 'empresa_bolsa'


class Ativo(models.Model):
    descricao = models.CharField(max_length=100, null=False)
    codigo = models.CharField(max_length=50, null=False)
    tipo_ativo = models.ForeignKey(TipoAtivo, on_delete=models.PROTECT)
    empresa = models.ForeignKey(EmpresaBolsa, on_delete=models.PROTECT, null=True)
    mercado = models.ForeignKey(Mercado, on_delete=models.PROTECT, null=True)
    setor = models.ForeignKey(Setor, on_delete=models.PROTECT, null=True)
    industria = models.ForeignKey(Industria, on_delete=models.PROTECT, null=True)
    bolsa = models.ForeignKey(Bolsa, on_delete=models.PROTECT)

    class Meta:
        db_table = 'ativo'


class Cotacao(models.Model):
    ativo = models.ForeignKey(Ativo, on_delete=models.PROTECT)
    data_pregao = models.DateField(null=True)
    data_hora_pregao = models.DateTimeField(null=True, blank=True)
    moeda = models.ForeignKey(Moeda, on_delete=models.PROTECT)
    preco_abertura = models.FloatField(null=False, default=0, blank=True)
    preco_atual = models.FloatField(blank=True, null=True)
    preco_maximo = models.FloatField(blank=True, null=True)
    preco_minimo = models.FloatField(blank=True, null=True)
    preco_ultimo = models.FloatField(blank=True, null=True)
    volume = models.IntegerField(blank=False, null=True)
    quantidade = models.FloatField(blank=False, null=True)
    valor_mercado = models.FloatField(blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=False, null=False)
    data_atualizacao = models.DateTimeField(blank=False, null=False)

    class Meta:
        db_table = 'cotacao'


class Posicao(models.Model):
    data_posicao = models.DateField(null=True)
    quantidade = models.IntegerField(null=True, blank=False)
    valor_operacao = models.FloatField(null=False, default=0, blank=True)
    valor_taxa_corretor = models.FloatField(null=True, default=0, blank=True)
    moeda = models.ForeignKey(Moeda, on_delete=models.PROTECT)
    ativo = models.ForeignKey(Ativo, on_delete=models.PROTECT)
    tipo_posicao = models.ForeignKey(TipoPosicao, on_delete=models.PROTECT)
    data_cadastro = models.DateTimeField(blank=True, null=False)
    data_atualizacao = models.DateTimeField(blank=False, null=True)
    status_ativo = models.BooleanField(default=True, blank=True)
    perfil_responsavel = models.ForeignKey(Perfil, on_delete=models.PROTECT, null=True)
    conta = models.ForeignKey(Conta, on_delete=models.PROTECT, null=True)

    class Meta:
        db_table = 'posicao'


class Portfolio(models.Model):
    descricao = models.CharField(max_length=100, null=False)
    data_cadastro = models.DateTimeField(blank=False, null=True)
    ativos = models.ManyToManyField(Ativo)
    perfil_responsavel = models.ForeignKey(Perfil, on_delete=models.PROTECT, null=True)

    class Meta:
        db_table = 'portfolio'
